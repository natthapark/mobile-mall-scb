package com.natthapark.app.mobilemall.ui.listmobile


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.natthapark.app.mobilemall.constant.Constant.LOW_TO_HIGH
import com.natthapark.app.mobilemall.data.model.MobileListResponse
import com.natthapark.app.mobilemall.data.repository.MobileListRepository
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MobileListViewModelTest {


    @Mock
    lateinit var mockRepository: MobileListRepository

    private lateinit var viewModel: MobileListViewModel

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        viewModel = MobileListViewModel(mockRepository)

    }

    @Test
    fun `Get mobile list success mobileListResponse live data should  not be null `() {
        val call = Mockito.mock(Call::class.java)

        Mockito.`when`(mockRepository.getMobileList()).thenReturn(call as Call<List<MobileListResponse>>)
        Mockito.doAnswer {
            val callback: Callback<List<MobileListResponse>> = it.getArgument(0)
            callback.onResponse(call, Response.success(listOf()))
        }.`when`(call).enqueue(Mockito.any())

        viewModel.getMobileList(LOW_TO_HIGH)

        Assert.assertNotNull(viewModel.mobileListResponse.value)
    }

    @Test
    fun `Get mobile list error mobileListResponse live data should be null `() {
        val call = Mockito.mock(Call::class.java)

        Mockito.`when`(mockRepository.getMobileList()).thenReturn(call as Call<List<MobileListResponse>>)
        Mockito.doAnswer {
            val callback: Callback<List<MobileListResponse>> = it.getArgument(0)
            callback.onResponse(call, Response.success(null))
        }.`when`(call).enqueue(Mockito.any())

        viewModel.getMobileList(LOW_TO_HIGH)

        Assert.assertNull(viewModel.mobileListResponse.value)
    }

}