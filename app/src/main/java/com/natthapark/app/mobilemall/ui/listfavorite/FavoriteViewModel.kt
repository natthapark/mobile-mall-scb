package com.natthapark.app.mobilemall.ui.listfavorite

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.natthapark.app.mobilemall.constant.Constant
import com.natthapark.app.mobilemall.data.model.MobileListResponse
import com.natthapark.app.mobilemall.data.repository.MobileListRepository

class FavoriteViewModel(private val repository: MobileListRepository) : ViewModel() {

    private var mobileList: List<MobileListResponse>? = null

    var favoriteList = MutableLiveData<List<MobileListResponse>>()

    fun setMobileListItem(item: List<MobileListResponse>?) {
        this.mobileList = item
    }

    fun getFavorite(sortingType: String) {
        mobileList?.let { list ->
            val favList = list.filter { repository.getFavorite(it.id.toString()) }
            favoriteList.value = sortData(sortingType, favList)
        }
    }

    fun setSortingType(sortingType: String) {
        favoriteList.value?.let {
            favoriteList.value = sortData(sortingType, it)
        }
    }

    private fun sortData(
        sortingType: String,
        list: List<MobileListResponse>
    ): List<MobileListResponse> {
        return when (sortingType) {
            Constant.LOW_TO_HIGH -> {
                list.sortedBy { it.price }
            }
            Constant.HIGH_TO_LOW -> {
                list.sortedByDescending { it.price }
            }
            Constant.RATING -> {
                list.sortedByDescending { it.rating }
            }
            else -> {
                list.sortedBy { it.price }
            }
        }
    }
}