package com.natthapark.app.mobilemall.ui.detail

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import com.natthapark.app.mobilemall.R
import com.natthapark.app.mobilemall.data.model.MobileImageResponse
import com.natthapark.app.mobilemall.data.model.MobileListResponse
import kotlinx.android.synthetic.main.activity_detail.*
import org.koin.android.viewmodel.ext.android.viewModel

const val EXTRA_IMAGE_ID = "EXTRA_IMAGE_ID"
const val EXTRA_MOBILE_RESPONSE = "EXTRA_MOBILE_RESPONSE"

class DetailActivity : AppCompatActivity() {

    private val detailViewModel: DetailViewModel by viewModel()
    private val imageId by lazy { intent.getStringExtra(EXTRA_IMAGE_ID) }
    private val mobileResponse by lazy { intent.getParcelableExtra(EXTRA_MOBILE_RESPONSE) as MobileListResponse }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setupToolbar()
        getMobileImage()
        setupObserver()
    }

    private fun setupToolbar() {
        val mTopToolbar = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(mTopToolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }


    private fun setupObserver() {
        detailViewModel.mobileImageListResponse.observe(this, Observer { data ->
            if (data != null) {
                setupViewPager(data)
                setupTextView()
            }
        })

        detailViewModel.isLoading.observe(this, Observer { data ->
            if (data != null) {
                if (data) {
                    progressBar.visibility = View.VISIBLE
                } else {
                    progressBar.visibility = View.GONE
                }
            }
        })
    }

    private fun setupTextView() {
        titleTextView.text = mobileResponse.name
        brandTextView.text = mobileResponse.brand
        descTextView.text = mobileResponse.description
    }

    private fun getMobileImage() {
        imageId?.let {
            detailViewModel.getMobileImage(it)
        }
    }

    private fun setupViewPager(item: List<MobileImageResponse>) {
        val viewPagerAdapter = DetailPagerAdapter(this, item)
        viewPager.adapter = viewPagerAdapter
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
