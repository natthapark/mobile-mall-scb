package com.natthapark.app.mobilemall.ui.listfavorite

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.natthapark.app.mobilemall.R
import com.natthapark.app.mobilemall.ui.detail.DetailActivity
import com.natthapark.app.mobilemall.ui.listmobile.EXTRA_IMAGE_ID
import com.natthapark.app.mobilemall.ui.listmobile.EXTRA_MOBILE_RESPONSE
import com.natthapark.app.mobilemall.ui.main.MainActivity
import kotlinx.android.synthetic.main.fragment_favorite_list.*
import org.koin.android.viewmodel.ext.android.viewModel


class FavoriteListFragment : Fragment() {

    private val favoriteViewModel: FavoriteViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favorite_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupObserver()
    }

    private fun setupViewModel() {
        favoriteViewModel.setMobileListItem((activity as MainActivity).listItem)
    }

    private fun setupObserver() {
        favoriteViewModel.favoriteList.observe(this, Observer { data ->
            if (data != null) {
                val mobileListAdapter = FavoriteListAdapter(data, this::onRecyclerViewClick)
                recyclerView.apply {
                    layoutManager = LinearLayoutManager(activity)
                    adapter = mobileListAdapter
                }
            }
        })
    }

    private fun onRecyclerViewClick(position: Int) {
        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra(EXTRA_IMAGE_ID, favoriteViewModel.favoriteList.value!![position].id.toString())
        intent.putExtra(EXTRA_MOBILE_RESPONSE, favoriteViewModel.favoriteList.value!![position])
        startActivity(intent)
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            setupViewModel()
            favoriteViewModel.getFavorite((activity as MainActivity).sortingType)
        } else {

        }
    }

    fun setSortingType(sortingType: String) {
        favoriteViewModel.setSortingType(sortingType)
    }

    companion object {
        fun newInstance() =
            FavoriteListFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}