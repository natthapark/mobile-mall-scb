package com.natthapark.app.mobilemall.data.api

import com.natthapark.app.mobilemall.data.model.MobileImageResponse
import com.natthapark.app.mobilemall.data.model.MobileListResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface Api {

    @GET("api/mobiles/")
    fun getMobileList(): Call<List<MobileListResponse>>

    @GET("api/mobiles/{mobile_id}/images/")
    fun getMobileImage(@Path("mobile_id") imageId: String): Call<List<MobileImageResponse>>

}