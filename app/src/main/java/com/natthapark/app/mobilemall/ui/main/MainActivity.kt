package com.natthapark.app.mobilemall.ui.main

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.android.material.tabs.TabLayout
import com.natthapark.app.mobilemall.R
import com.natthapark.app.mobilemall.constant.Constant.LOW_TO_HIGH
import com.natthapark.app.mobilemall.data.model.MobileListResponse
import com.natthapark.app.mobilemall.widget.SortingDialog
import kotlinx.android.synthetic.main.activity_main.*


const val MOBILE_LIST = "MOBILE LIST"
const val FAVORITE_LIST = "FAVORITE LIST"

class MainActivity : AppCompatActivity() {

    var listItem: List<MobileListResponse>? = null
    var sortingType = LOW_TO_HIGH
    var mainAdapter: MainAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mTopToolbar = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(mTopToolbar)

        setupViewPager()
        setupTabLayout()
    }

    private fun setupViewPager() {
        mainAdapter = MainAdapter(supportFragmentManager)
        viewPager?.adapter = mainAdapter
    }

    private fun setupTabLayout() {
        tabLayout?.addTab(tabLayout.newTab().setText(MOBILE_LIST))
        tabLayout?.addTab(tabLayout.newTab().setText(FAVORITE_LIST))
        tabLayout?.tabGravity = TabLayout.GRAVITY_FILL
        tabLayout?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                viewPager?.currentItem = tab?.position ?: 0
            }
        })
    }

    private fun onRadioSelected(sortingType: String) {
        this.sortingType = sortingType
        mainAdapter?.setSortingType(sortingType)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        if (id == R.id.action_sort) {
            val dialog = SortingDialog.newInstance(sortingType)
            dialog.setOnRadioSelectedListener(this::onRadioSelected)
            dialog.show(supportFragmentManager, "Sorting Dialog")
            return true
        }

        return super.onOptionsItemSelected(item)
    }
}
