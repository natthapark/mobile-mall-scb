package com.natthapark.app.mobilemall.ui.listfavorite

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.natthapark.app.mobilemall.R
import com.natthapark.app.mobilemall.data.model.MobileListResponse
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_favorite.*

class FavoriteListAdapter(private val item: List<MobileListResponse>,
                          private val onClick: (position: Int) -> Unit) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return FavoriteItemViewHolder.create(parent.context, parent)
    }

    override fun getItemCount(): Int {
        return item.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is FavoriteItemViewHolder ->
                holder.bind(item[position], position, onClick)
        }
    }
}


class FavoriteItemViewHolder(
    override val containerView: View
) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun bind(
        mobileListResponse: MobileListResponse,
        position: Int,
        onClick: (position: Int) -> Unit
    ) {

        titleTextView.text = mobileListResponse.name
        priceTextView.text = "Price: $${mobileListResponse.price}"
        ratingTextView.text = "Rating: ${mobileListResponse.rating}"

        Glide
            .with(containerView.context)
            .load(mobileListResponse.thumbImageURL)
            .centerCrop()
            .into(profileImageView)

        rootLayout.setOnClickListener {
            onClick.invoke(position)
        }

    }


    companion object {
        fun create(context: Context, parent: ViewGroup): FavoriteItemViewHolder {
            return FavoriteItemViewHolder(
                LayoutInflater.from(context).inflate(R.layout.item_favorite, parent, false)
            )
        }

    }
}