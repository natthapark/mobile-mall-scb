package com.natthapark.app.mobilemall.constant

object Constant {

    const val ENDPOINT_API = "https://scb-test-mobile.herokuapp.com/"

    /**
     * Sorting type
     */
    const val LOW_TO_HIGH = "LOW_TO_HIGH"
    const val HIGH_TO_LOW = "HIGH_TO_LOW"
    const val RATING = "RATING"

}