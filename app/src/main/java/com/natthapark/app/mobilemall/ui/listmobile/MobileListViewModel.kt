package com.natthapark.app.mobilemall.ui.listmobile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.natthapark.app.mobilemall.constant.Constant.HIGH_TO_LOW
import com.natthapark.app.mobilemall.constant.Constant.LOW_TO_HIGH
import com.natthapark.app.mobilemall.constant.Constant.RATING
import com.natthapark.app.mobilemall.data.model.MobileListResponse
import com.natthapark.app.mobilemall.data.repository.MobileListRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MobileListViewModel(private val repository: MobileListRepository) : ViewModel() {

    var mobileListResponse = MutableLiveData<List<MobileListResponse>>()
    var isLoading = MutableLiveData<Boolean>()

    fun getMobileList(sortingType: String) {
        isLoading.value = true
        repository.getMobileList().enqueue(object : Callback<List<MobileListResponse>> {
            override fun onFailure(call: Call<List<MobileListResponse>>, t: Throwable) {
                isLoading.value = false
                mobileListResponse.value = null
            }

            override fun onResponse(
                call: Call<List<MobileListResponse>>,
                response: Response<List<MobileListResponse>>
            ) {
                response.body()?.let {
                    isLoading.value = false
                    mobileListResponse.value = sortData(sortingType, it)
                }
            }
        })
    }

    fun saveFavorite(item: MobileListResponse, isFavorite: Boolean) {
        repository.saveFavorite(item, isFavorite)
    }

    fun setSortingType(sortingType: String) {
        mobileListResponse.value?.let {
            mobileListResponse.value = sortData(sortingType, it)
        }
    }

    private fun sortData(
        sortingType: String,
        list: List<MobileListResponse>
    ): List<MobileListResponse> {
        return when (sortingType) {
            LOW_TO_HIGH -> {
                list.sortedBy { it.price }
            }
            HIGH_TO_LOW -> {
                list.sortedByDescending { it.price }
            }
            RATING -> {
                list.sortedByDescending { it.rating }
            }
            else -> {
                list.sortedBy { it.price }
            }
        }
    }

}