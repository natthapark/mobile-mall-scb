package com.natthapark.app.mobilemall

import android.app.Application
import com.natthapark.app.mobilemall.di.appModule
import org.koin.android.ext.android.startKoin

class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin(this, listOf(appModule))
    }
}