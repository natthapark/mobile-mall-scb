package com.natthapark.app.mobilemall.data.repository

import com.natthapark.app.mobilemall.data.model.MobileImageResponse
import com.natthapark.app.mobilemall.data.model.MobileListResponse
import retrofit2.Call

interface MobileListRepository {

    fun getMobileList(): Call<List<MobileListResponse>>

    fun getMobileImage(imageId: String): Call<List<MobileImageResponse>>

    fun saveFavorite(item: MobileListResponse, isFavorite: Boolean)

    fun getFavorite(id: String): Boolean
}