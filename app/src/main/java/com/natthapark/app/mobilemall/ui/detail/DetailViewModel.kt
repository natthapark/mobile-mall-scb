package com.natthapark.app.mobilemall.ui.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.natthapark.app.mobilemall.data.model.MobileImageResponse
import com.natthapark.app.mobilemall.data.repository.MobileListRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailViewModel(private val repository: MobileListRepository) : ViewModel() {

    var mobileImageListResponse = MutableLiveData<List<MobileImageResponse>>()
    var isLoading = MutableLiveData<Boolean>()

    fun getMobileImage(imageId: String) {
        isLoading.value = true
        repository.getMobileImage(imageId).enqueue(object : Callback<List<MobileImageResponse>> {
            override fun onFailure(call: Call<List<MobileImageResponse>>, t: Throwable) {
                isLoading.value = false
                mobileImageListResponse.value = null
            }

            override fun onResponse(
                call: Call<List<MobileImageResponse>>,
                response: Response<List<MobileImageResponse>>
            ) {
                if (response.isSuccessful) {
                    isLoading.value = false
                    mobileImageListResponse.value = response.body()
                }
            }
        })
    }

}