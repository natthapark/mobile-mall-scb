package com.natthapark.app.mobilemall.di

import com.natthapark.app.mobilemall.constant.Constant
import com.natthapark.app.mobilemall.data.api.Api
import com.natthapark.app.mobilemall.data.repository.MobileListRepository
import com.natthapark.app.mobilemall.data.repository.MobileListRepositoryImpl
import com.natthapark.app.mobilemall.ui.detail.DetailViewModel
import com.natthapark.app.mobilemall.ui.listfavorite.FavoriteViewModel
import com.natthapark.app.mobilemall.ui.listmobile.MobileListViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val interceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
    this.level = HttpLoggingInterceptor.Level.BODY
}

val okHttpClient: OkHttpClient? = OkHttpClient.Builder()
    .addInterceptor(interceptor)
    .build()

val appModule = module {
    val endpoint = Constant.ENDPOINT_API
    val retrofit = Retrofit.Builder()
        .baseUrl(endpoint)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
    val api: Api = retrofit.create(Api::class.java)

    single(definition = { MobileListRepositoryImpl(androidContext(), api) as MobileListRepository })

    viewModel { MobileListViewModel(get()) }
    viewModel { DetailViewModel(get()) }
    viewModel { FavoriteViewModel(get()) }
}

