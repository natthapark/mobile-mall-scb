package com.natthapark.app.mobilemall.ui.detail

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.natthapark.app.mobilemall.R
import com.natthapark.app.mobilemall.data.model.MobileImageResponse


class DetailPagerAdapter(private val context: Context, private val item: List<MobileImageResponse>) : PagerAdapter() {

    private var layoutInflater: LayoutInflater? = null

    init {

        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as LinearLayout
    }

    override fun getCount(): Int {
        return item.size
    }

    override fun getPageWidth(position: Int): Float {
        return 0.5f
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = layoutInflater?.inflate(R.layout.item_detail, container, false)
        val imageView = itemView?.findViewById<ImageView>(R.id.screenImageView)
        imageView?.let {
            Glide
                .with(context)
                .load(item[position].url)
                .fitCenter()
                .into(it)
        }

        container.addView(itemView)
        return itemView!!
    }

}