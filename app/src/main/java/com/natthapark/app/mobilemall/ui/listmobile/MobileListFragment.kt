package com.natthapark.app.mobilemall.ui.listmobile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.natthapark.app.mobilemall.R
import com.natthapark.app.mobilemall.data.model.MobileListResponse
import com.natthapark.app.mobilemall.ui.detail.DetailActivity
import com.natthapark.app.mobilemall.ui.main.MainActivity
import kotlinx.android.synthetic.main.fragment_mobile_list.*
import org.koin.android.viewmodel.ext.android.viewModel

const val EXTRA_IMAGE_ID = "EXTRA_IMAGE_ID"
const val EXTRA_MOBILE_RESPONSE = "EXTRA_MOBILE_RESPONSE"

class MobileListFragment : Fragment() {

    private val mobileListViewModel: MobileListViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_mobile_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getMobileList()
        setupObserver()
    }

    private fun getMobileList() {
        mobileListViewModel.getMobileList((activity as MainActivity).sortingType)
    }

    private fun setupObserver() {
        mobileListViewModel.mobileListResponse.observe(this, Observer { data ->
            if (data != null) {
                (activity as MainActivity).listItem = data
                setupRecyclerView(data)
            }
        })

        mobileListViewModel.isLoading.observe(this, Observer { data ->
            if (data != null) {
                if (data) {
                    progressBar.visibility = View.VISIBLE
                } else {
                    progressBar.visibility = View.GONE
                }
            }
        })
    }

    private fun setupRecyclerView(data: List<MobileListResponse>) {
        val mobileListAdapter = MobileListAdapter(context!!, data, this::onRecyclerViewClick, this::onClickFavorite)
        recyclerView.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = mobileListAdapter
        }
    }

    private fun onClickFavorite(position: Int, isFavorite: Boolean) {
        mobileListViewModel.mobileListResponse.value?.let {
            mobileListViewModel.saveFavorite(it[position], isFavorite)
        }
    }

    private fun onRecyclerViewClick(position: Int) {
        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra(EXTRA_IMAGE_ID, mobileListViewModel.mobileListResponse.value!![position].id.toString())
        intent.putExtra(EXTRA_MOBILE_RESPONSE, mobileListViewModel.mobileListResponse.value!![position])
        startActivity(intent)
    }

    fun setSortingType(sortingType: String) {
        mobileListViewModel.setSortingType(sortingType)
    }

    companion object {
        fun newInstance() =
            MobileListFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}