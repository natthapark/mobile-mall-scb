package com.natthapark.app.mobilemall.data.repository

import android.content.Context
import com.natthapark.app.mobilemall.data.api.Api
import com.natthapark.app.mobilemall.data.model.MobileImageResponse
import com.natthapark.app.mobilemall.data.model.MobileListResponse
import retrofit2.Call

const val PREF_FAVORITE = "PREF_FAVORITE"
const val KEY_FAVORITE = "KEY_FAVORITE"

class MobileListRepositoryImpl(private val context: Context, private val api: Api) : MobileListRepository {

    private val sharedPreferences by lazy {
        context.getSharedPreferences(PREF_FAVORITE, Context.MODE_PRIVATE)
    }

    override fun getMobileList(): Call<List<MobileListResponse>> {
        return api.getMobileList()
    }

    override fun getMobileImage(imageId: String): Call<List<MobileImageResponse>> {
        return api.getMobileImage(imageId)
    }

    override fun saveFavorite(item: MobileListResponse, isFavorite: Boolean) {

        sharedPreferences.edit().apply {
            putBoolean(KEY_FAVORITE + "_" + item.id, isFavorite)
        }.apply()
    }

    override fun getFavorite(id: String): Boolean {
        return sharedPreferences.getBoolean(KEY_FAVORITE+"_"+id, false)
    }
}