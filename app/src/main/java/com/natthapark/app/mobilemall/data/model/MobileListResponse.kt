package com.natthapark.app.mobilemall.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MobileListResponse(
    val id: Int,
    val name: String,
    val thumbImageURL: String,
    val description: String,
    val price: Double,
    val brand: String,
    val rating: Double
): Parcelable