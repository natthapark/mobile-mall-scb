package com.natthapark.app.mobilemall.ui.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.natthapark.app.mobilemall.ui.listfavorite.FavoriteListFragment
import com.natthapark.app.mobilemall.ui.listmobile.MobileListFragment

class MainAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private var mobileFragment: MobileListFragment = MobileListFragment.newInstance()
    private var favoriteFragment = FavoriteListFragment.newInstance()

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> mobileFragment
            1 -> favoriteFragment
            else -> mobileFragment
        }
    }

    override fun getCount(): Int = 2

    fun setSortingType(sortingType: String) {
        mobileFragment.setSortingType(sortingType)
        favoriteFragment.setSortingType(sortingType)
    }

}