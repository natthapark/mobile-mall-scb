package com.natthapark.app.mobilemall.ui.listmobile

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.natthapark.app.mobilemall.R
import com.natthapark.app.mobilemall.data.model.MobileListResponse
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_mobile.*

class MobileListAdapter(
    private val context: Context,
    private val item: List<MobileListResponse>,
    private val onClick: (position: Int) -> Unit,
    private val onClickFavorite: (position: Int, isFavorite: Boolean) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MobileItemViewHolder.create(parent.context, parent)
    }

    override fun getItemCount(): Int {
        return item.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is MobileItemViewHolder ->
                holder.bind(context, item[position], position, onClick, onClickFavorite)
        }
    }
}


const val PREF_FAVORITE = "PREF_FAVORITE"
const val KEY_FAVORITE = "KEY_FAVORITE"

class MobileItemViewHolder(
    override val containerView: View
) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    private var isFavorite = false

    fun bind(
        context: Context,
        mobileListResponse: MobileListResponse,
        position: Int,
        onClick: (position: Int) -> Unit,
        onClickFavorite: (position: Int, isFavorite: Boolean) -> Unit
    ) {

        titleTextView.text = mobileListResponse.name
        descTextView.text = mobileListResponse.description
        priceTextView.text = "Price: $${mobileListResponse.price}"
        ratingTextView.text = "Rating: ${mobileListResponse.rating}"

        Glide
            .with(containerView.context)
            .load(mobileListResponse.thumbImageURL)
            .centerCrop()
            .into(profileImageView)

        val sharedPreferences = context.getSharedPreferences(PREF_FAVORITE, Context.MODE_PRIVATE)
        val isFavoriteFromPreference = sharedPreferences.getBoolean(KEY_FAVORITE + "_" + mobileListResponse.id, false)
        isFavorite = isFavoriteFromPreference

        if (isFavoriteFromPreference) {
            favoriteImageView.setImageResource(android.R.drawable.btn_star_big_on)
        } else {
            favoriteImageView.setImageResource(android.R.drawable.btn_star_big_off)
        }


        favoriteImageView.setOnClickListener {
            if (isFavorite) {
                favoriteImageView.setImageResource(android.R.drawable.btn_star_big_on)
            } else {
                favoriteImageView.setImageResource(android.R.drawable.btn_star_big_off)
            }
            onClickFavorite.invoke(position, isFavorite)
            isFavorite = !isFavorite

        }

        rootLayout.setOnClickListener {
            onClick.invoke(position)
        }
    }


    companion object {
        fun create(context: Context, parent: ViewGroup): MobileItemViewHolder {
            return MobileItemViewHolder(
                LayoutInflater.from(context).inflate(R.layout.item_mobile, parent, false)
            )
        }

    }
}