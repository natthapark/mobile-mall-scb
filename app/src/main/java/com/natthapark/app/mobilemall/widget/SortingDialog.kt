package com.natthapark.app.mobilemall.widget

import android.media.Rating
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.natthapark.app.mobilemall.R
import com.natthapark.app.mobilemall.constant.Constant.HIGH_TO_LOW
import com.natthapark.app.mobilemall.constant.Constant.LOW_TO_HIGH
import com.natthapark.app.mobilemall.constant.Constant.RATING
import kotlinx.android.synthetic.main.dialog_sorting.*

const val ARGUMENT_SORTING_TYPE = "ARGUMENT_SORTING_TYPE"

class SortingDialog : DialogFragment() {

    private val sortingType by lazy {
        arguments?.getString(ARGUMENT_SORTING_TYPE)
    }

    private var onRadioSelectedListener: ((sortingType: String) -> Unit)? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val inflate = activity?.layoutInflater
        return inflate?.inflate(R.layout.dialog_sorting, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRadioButton()
    }

    private fun setupRadioButton() {

        when (sortingType) {
            LOW_TO_HIGH -> {
                lowToHighRadioButton.isChecked = true
            }
            HIGH_TO_LOW -> {
                highToLowRadioButton.isChecked = true
            }
            RATING -> {
                ratingRadioButton.isChecked = true
            }
        }

        sortingRadioGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.lowToHighRadioButton -> {
                    onRadioSelectedListener?.invoke(LOW_TO_HIGH)
                }
                R.id.highToLowRadioButton -> {
                    onRadioSelectedListener?.invoke(HIGH_TO_LOW)
                }
                R.id.ratingRadioButton -> {
                    onRadioSelectedListener?.invoke(RATING)
                }
            }
            dismiss()
        }
    }

    fun setOnRadioSelectedListener(onRadioSelectedListener: ((sortingType: String) -> Unit)?) {
        this.onRadioSelectedListener = onRadioSelectedListener
    }

    companion object {
        fun newInstance(sortingType: String) =
            SortingDialog().apply {
                arguments = Bundle().apply {
                    putString(ARGUMENT_SORTING_TYPE, sortingType)
                }
            }
    }
}