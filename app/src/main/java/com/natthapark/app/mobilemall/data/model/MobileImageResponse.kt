package com.natthapark.app.mobilemall.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MobileImageResponse(
    val url: String,
    val id: Int,
    val mobile_id: Int
) : Parcelable